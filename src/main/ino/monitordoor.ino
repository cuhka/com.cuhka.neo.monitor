static const int LED_INDICATION_OPENED  = 9;
static const int LED_INDICATION_CLOSED = 8;
static const int LED_OUTSIDE = 10;
static const int DOOR_SENSOR = 2;
static const int OUTSIDE_ALERT_TIMEOUT = 5000;
static const int FLASH_LENGTH = 250;
boolean wasClosed;
long timeout = 0;
long flash = 0;
boolean flashOn;

void setup() {
  pinMode(LED_INDICATION_CLOSED, OUTPUT);
  pinMode(LED_INDICATION_OPENED, OUTPUT);
  pinMode(LED_OUTSIDE, OUTPUT);
  pinMode(DOOR_SENSOR, INPUT);

  // initialize the opposite way so
  // that on first read the current
  // state will be written to the 
  // serial. 
  wasClosed = !readDoorState();
  Serial.begin(115200);
}

void loop() {
  const boolean isClosed = readDoorState();
  const boolean changed = wasClosed != isClosed;

  wasClosed = isClosed;
  if (changed) {
    digitalWrite(LED_INDICATION_CLOSED, isClosed ? HIGH : LOW);
    digitalWrite(LED_INDICATION_OPENED, isClosed ? LOW : HIGH);

    if (isClosed) { 
      digitalWrite(LED_OUTSIDE, LOW);
      timeout = 0;
    } else {
      digitalWrite(LED_OUTSIDE, HIGH);
      flashOn = true;
      timeout = millis() + OUTSIDE_ALERT_TIMEOUT;
    }

    if (Serial.availableForWrite() >= 1) {
      Serial.write(isClosed ? 'C' : 'O');
    }
  }

  if (timeout != 0 && millis() >= timeout) {
    if (flash == 0 || flash <= millis()) {
      digitalWrite(LED_OUTSIDE, flashOn ? HIGH : LOW);
      flash = millis() + FLASH_LENGTH;
      flashOn = !flashOn;
    }
  }
}

boolean readDoorState() {
  return digitalRead(DOOR_SENSOR) == HIGH;
}

