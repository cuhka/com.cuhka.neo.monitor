package com.cuhka.neo.monitor;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class DoorMonitor {
	private final static MqttMessage DOOR_OPEN;
	private final static MqttMessage DOOR_CLOSED;
	
	private final Logger log = Logger.getLogger(DoorMonitor.class.getName());
	private final MqttAsyncClient client;
	private final String topic;
	private final ArduinoReader reader;
	private ScheduledFuture<?> scheduledFuture;

	static {
		DOOR_OPEN = new MqttMessage("open".getBytes(StandardCharsets.US_ASCII));
		DOOR_OPEN.setRetained(true);
		
		DOOR_CLOSED = new MqttMessage("closed".getBytes(StandardCharsets.US_ASCII));
		DOOR_CLOSED.setRetained(true);
	}
	
	public DoorMonitor(MqttAsyncClient mqttClient, ArduinoReader arduino, String topic) {
		this.client = Objects.requireNonNull(mqttClient);
		this.reader = Objects.requireNonNull(arduino);
		this.topic = Objects.requireNonNull(topic);
	}

	public void start(ScheduledExecutorService service) {
		scheduledFuture = service.scheduleWithFixedDelay(this::poll, 2, 500, TimeUnit.MILLISECONDS);
	}
	
	public void stop() {
		scheduledFuture.cancel(false);
		
		try {
			client.publish(topic, new byte[0], 0, true);
		} catch (MqttException e) {
			log.log(Level.WARNING, "While erasing retained message", e);
		}
	}

	private void publishState(DoorState doorstate) {
		MqttMessage message;
		
		switch (doorstate) {
		case OPEN:
			message = DOOR_OPEN;
			break;
			
		case CLOSED:
			message = DOOR_CLOSED;
			break;
			
		default:
			throw new AssertionError("Unhandled state " + doorstate);
		}
		
		try {
			client.publish(topic, message);
		} catch (MqttException e) {
			log.log(Level.SEVERE, "while publishing door state", e);
		}
	}
	
	private void poll() {
		try {
			reader.readNextState().ifPresent(this::publishState);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}