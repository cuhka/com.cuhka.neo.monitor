package com.cuhka.neo.monitor;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;

import com.cuhka.neo.sensor.mpl3115a2.Barometer;

public class TemperatureMonitor extends SensorMonitor {
	private final Barometer sensor;

	public TemperatureMonitor(MqttAsyncClient mqttClient, String topic) {
		super(mqttClient, topic);
		this.sensor = new Barometer();
	}

	@Override
	protected double readNewValue() {
		return sensor.readTemperature();
	}
}
