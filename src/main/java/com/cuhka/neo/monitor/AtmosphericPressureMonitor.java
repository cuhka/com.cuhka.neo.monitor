package com.cuhka.neo.monitor;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;

import com.cuhka.neo.sensor.mpl3115a2.Barometer;

public class AtmosphericPressureMonitor extends SensorMonitor {
	private final Barometer sensor;
	
	public AtmosphericPressureMonitor(MqttAsyncClient client, String topic) {
		super(client, topic);
		sensor = new Barometer();
	}

	@Override
	protected double readNewValue() {
		return sensor.readPressure();
	}

}
