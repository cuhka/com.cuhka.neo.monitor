package com.cuhka.neo.monitor;

import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;


class ThreadCreator implements ThreadFactory {
	private final Logger logger = Logger.getLogger(ThreadCreator.class.getName());
	private final ThreadGroup threadGroup;
	private int counter = 0;
	
	ThreadCreator() {
		threadGroup = new ThreadGroup("Polling threads") {
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				logger.log(Level.SEVERE, "Unhandled exception in " + t.getName(), e);
				super.uncaughtException(t, e);
			}
		};
		
		threadGroup.setDaemon(true);
	}
		
	@Override
	public Thread newThread(Runnable r) {
		return new Thread(threadGroup, r, "Polling Executor " + ++counter);
	}
}
