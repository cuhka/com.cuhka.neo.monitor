package com.cuhka.neo.monitor;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceException;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;

public class MonitorActivator implements BundleActivator, MqttCallback {
	private static final String GARAGE = "home/garage";
	private MqttAsyncClient mqttClient;
	private MqttConnectOptions connectOptions;
	private ScheduledExecutorService service;
	private MqttDefaultFilePersistence persistence;
	private final Logger logger = Logger.getLogger(MonitorActivator.class.getName());
	private ArduinoReader arduino;
	private DoorMonitor doorMonitor;

	@Override
	public void start(BundleContext context) throws Exception {
		try {
			configureMqtt(context);

			service = createExecutor();
			arduino = createArduinoReader(context);
			startMonitoring();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Could not start bundle correctly", e);
			if (mqttClient != null && mqttClient.isConnected()) {
				mqttClient.disconnect();
			}
			
			if (persistence != null) {
				persistence.close();
			}

			throw e;
		}
	}

	private void configureMqtt(BundleContext context) throws IOException, MqttException {
		String broker = context.getProperty("mqtt.broker");
		
		if (broker == null) {
			throw new ServiceException("Define mqtt.broker");
		}
		
		String client = context.getProperty("mqtt.clientname");
		
		if (client == null) {
			throw new ServiceException("Define mqtt.clientname");
		}
		
		String dir = context.getProperty("mqtt.persistence");
		
		if (dir == null) {
			dir = "mqtt";
		};
		
		File directory = context.getDataFile(dir);
		directory.mkdirs();
		
		logger.config(() -> String.format("MQTT broker: %s, clientname: %s persistence: %s", broker, client, directory.getAbsolutePath()));
		connectOptions = new MqttConnectOptions();
		connectOptions.setWill(GARAGE + "/door", new byte[0], 0, true);
		
		String ka = context.getProperty("mqtt.keepalive");
		if (ka != null) {
			connectOptions.setKeepAliveInterval(Integer.valueOf(ka));
		}
		
		persistence = new MqttDefaultFilePersistence(directory.getCanonicalPath());
		mqttClient = new MqttAsyncClient(broker, client, persistence);
		mqttClient.setCallback(this);
		connect();
	}

	private ArduinoReader createArduinoReader(BundleContext context) throws ServiceException {
		CommPortIdentifier identifier = determineSerialPort(context);

		logger.config(() -> String.format("Use port %s", identifier.getName()));
		
		try {
			return new ArduinoReader(identifier, 2_000);
		} catch (ClassCastException | PortInUseException | IOException e) {
			logger.log(Level.SEVERE, "Can't create create ArduinoReader", e);
			throw new ServiceException("Cannot use serial port " + identifier.getName(), e);
		}
	}

	private CommPortIdentifier determineSerialPort(BundleContext context) {
		String portName = context.getProperty("arduino.serial");
		CommPortIdentifier identifier = null;
		
		if (portName != null) {
			try {
				identifier = CommPortIdentifier.getPortIdentifier(portName);
			} catch (NoSuchPortException e) {
				throw new ServiceException(String.format("Port %s does not exist on this system", portName));
			}
		} else {
			logger.fine("No port defined, search for system serial port");
			identifier = findSerialPort().orElse(null);
		}
		
		if (identifier == null) {
			throw new ServiceException("No serial port found on system");
		} 
		
		if (identifier.getPortType() != CommPortIdentifier.PORT_SERIAL) {
			throw new ServiceException(String.format("Port %s is not a serial port", identifier.getName()));
		}
		
		if (identifier.isCurrentlyOwned()) {
			throw new ServiceException(String.format("Port %s is currently in use by %s", identifier.getName(), identifier.getCurrentOwner()));
		}
		
		return identifier;
	}

	private Optional<CommPortIdentifier> findSerialPort() {
		@SuppressWarnings("unchecked")
		Enumeration<CommPortIdentifier> identifiers = CommPortIdentifier.getPortIdentifiers();
		
		Optional<CommPortIdentifier> identifier = findSerial(identifiers);
		
		if (findSerial(identifiers).isPresent()) {
			throw new ServiceException("System has multiple serial ports, define the port to use with property arduino.serial");
		}

		return identifier;
	}
	
	private Optional<CommPortIdentifier> findSerial(Enumeration<CommPortIdentifier> identifiers) {
		while (identifiers.hasMoreElements()) {
			CommPortIdentifier identifier = identifiers.nextElement();
			
			if (identifier.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				return Optional.of(identifier);
			}
		}
		
		return Optional.empty();
	}
	
	private ScheduledExecutorService createExecutor() {
		return Executors.unconfigurableScheduledExecutorService(
				Executors.newScheduledThreadPool(2 * Runtime.getRuntime().availableProcessors(), new ThreadCreator())
				);
	}

	private void startMonitoring() {
		new TemperatureMonitor(mqttClient, GARAGE + "/temperature").start(service);
		new AtmosphericPressureMonitor(mqttClient, GARAGE + "/atmospheric_pressure").start(service);
		doorMonitor = new DoorMonitor(mqttClient, arduino, GARAGE + "/door");
		doorMonitor.start(service);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		logger.fine("Stopping sensor monitoring bundle");
		arduino.close();
		service.shutdownNow();
		mqttClient.setCallback(null);
		if (mqttClient.isConnected()) {
			mqttClient.disconnect(3_000L);
		}
		
		persistence.close();
		service.awaitTermination(3, TimeUnit.SECONDS);
	}

	@Override
	public void connectionLost(Throwable cause) {
		logger.log(Level.SEVERE, "Connection to MQTT broker lost", cause);
		service.schedule(() -> connect(), 3, TimeUnit.SECONDS);
	}

	private void connect() {
		logger.fine("Connecting to MQTT broker");

		try {
			IMqttToken token = mqttClient.connect(connectOptions);

			IMqttActionListener listener = new IMqttActionListener() {
				@Override
				public void onSuccess(IMqttToken asyncActionToken) {
					logger.log(Level.INFO, "Connected to MQTT broker");
				}

				@Override
				public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
					logger.log(Level.WARNING, "Could not connect to MQTT broker, retrying in 3 seconds", exception);
					service.schedule(() -> connect(), 3, TimeUnit.SECONDS);
				}
			};

			token.setActionCallback(listener);
		} catch (MqttException e) {
			logger.log(Level.SEVERE, "Cannot reconnect to MQTT broker, giving up", e);
		}
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		logger.finest(() -> String.format("Message on topic %s arrived", topic));
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		String[] topics = token.getTopics();

		if (topics != null) {
			logger.finest(() -> String.format("Message %x (%s) delivered to broker", token.getMessageId(), 
					Arrays.stream(topics).collect(Collectors.joining(", "))));
		}

		MqttException error = token.getException();

		if (error != null) {
			logger.log(Level.SEVERE, String.format("MQTT delivery of %s had an error", token), error);
		}
	}
}
