package com.cuhka.neo.monitor;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public abstract class SensorMonitor {
	protected final MqttAsyncClient client;
	protected final String topic;
	protected final Logger logger = Logger.getLogger(getClass().getName());
	private final ByteArrayOutputStream byteOut = new ByteArrayOutputStream(8);
	private final DataOutputStream dataOut = new DataOutputStream(byteOut);
	
	public SensorMonitor(MqttAsyncClient mqttClient, String topic) {
		this.client = mqttClient;
		this.topic = topic;
	}

	public void start(ScheduledExecutorService service) {
		service.scheduleWithFixedDelay(() -> publish(), 0, 30, TimeUnit.SECONDS);
	}

	protected void publish() {
		try {
			MqttMessage message = toMesssage(readNewValue());
			client.publish(topic, message);
			
		} catch (MqttException | IOException e) {
			logger.log(Level.SEVERE, "Could not publish message", e);
		}
	}

	private MqttMessage toMesssage(double value) throws IOException {
		byteOut.reset();
		dataOut.writeDouble(value);
		return new MqttMessage(byteOut.toByteArray());
	}

	protected abstract double readNewValue();
}
